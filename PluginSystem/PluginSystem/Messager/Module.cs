﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginCommunication
{
    public class Module
    {
        private bool _ready;

        private Messenger _messenger;
        public Messenger Messenger
        {
            get => _messenger;
        }

        public event EventHandler<EventMessageData> _triggerEvent;

        private List<string> _allowedTypes;
        public List<string> AllowedTypes
        {
            get => _allowedTypes;
        }
    
        public Module()
        {
            _ready = false;
            _allowedTypes = new List<string>();
            init();
        }

        internal void GotRegisterd(Messenger Messenger)
        {
            _messenger = Messenger;
        }

        public virtual void init()
        {
        }

        protected void addType(string newType)
        {
            newType = newType.ToLower();
            if (!_allowedTypes.Contains(newType))
                _allowedTypes.Add(newType);
        }
        public virtual void Notified(MessageData DataSet)
        {
            if (!_ready)
                return;
            if (_allowedTypes.Contains(DataSet.Type))
                _triggerEvent?.Invoke(this, new EventMessageData(DataSet));
        }

        public void SendMessage(MessageData DataSet)
        {
            if (!_ready)
                return;
            _messenger.Notify(DataSet);
        }

        public void SendMessage(string Type, Object Data)
        {
            MessageData DataSet = new MessageData(this, Type, Data);
            SendMessage(DataSet);
        }
    }

}
 