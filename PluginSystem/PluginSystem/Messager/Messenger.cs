﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginCommunication
{
    public class Messenger
    {
        private List<Module> _registerdModules;
        public List<Module> RegisterdModules
        {
            get => _registerdModules;
        }

        private List<string> _allowedTypes
        {
            get
            {
                List<string> tempList = new List<string>();
                for (int i = 0; i < _registerdModules.Count; i++)
                {
                    if (_registerdModules[i] == null)
                    {
                        continue;
                    }

                    foreach (string type in _registerdModules[i].AllowedTypes)
                    {
                        tempList.Add(type);
                    }
                }
                return tempList;
            }
        }
        public List<string> AllowedTypes
        {
            get => _allowedTypes;
        }

        public Messenger()
        {
            _registerdModules = new List<Module>();
        }

        public void Register(Module _newModule)
        {
            for (int i = 0; i < _registerdModules.Count; i++)
            {
                if (_registerdModules[i] == _newModule)
                    return;
            }

            _newModule.GotRegisterd(this);
            _registerdModules.Add(_newModule);
        }

        public void Register(params Module[] _newModules)
        {
            foreach (Module module in _newModules)
            {
                Register(module);
            }
        }

        public void Unregister(params Module[] _deleteModules)
        {
            foreach (Module module in _deleteModules)
            {
                Unregister(module);
            }
        }

        public void Unregister(Module _deleteModule)
        {
            for (int i = 0; i < _registerdModules.Count; i++)
            {
                if (_registerdModules[i] == _deleteModule)
                {
                    _registerdModules.RemoveAt(i);
                    return;
                } 
            }
        }

        public void Notify(MessageData Message)
        {
            if (!_allowedTypes.Contains(Message.Type))
                return;
            for (int i = 0; i < _registerdModules.Count; i++)
            {
                if (_registerdModules[i] == null || _registerdModules[i] == Message.Sender)
                    continue;
                if (_registerdModules[i].AllowedTypes.Contains(Message.Type))
                    _registerdModules[i].Notified(Message);
            }
        }
    }
}
