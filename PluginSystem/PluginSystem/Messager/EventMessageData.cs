﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginCommunication
{
    public class EventMessageData : EventArgs
    {
        private MessageData _dataSet;

        public Object Data
        {
            get => _dataSet.Data;
        }
        public Type DataType
        {
            get => _dataSet.DataType;
        }

        public Module Sender
        {
            get => _dataSet.Sender;
        }

        public string Type
        {
            get => _dataSet.Type;
        }

        public EventMessageData(MessageData DataSet)
        {
            _dataSet = DataSet;
        }
    }
}
