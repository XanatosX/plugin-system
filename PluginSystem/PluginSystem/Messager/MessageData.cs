﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginCommunication
{
    public class MessageData
    {
        private Object _data;
        public Object Data
        {
            get => _data;
        }

        private Type _dataType;
        public Type DataType
        {
            get => _dataType;
        }

        private Module _sender;
        public Module Sender
        {
            get => _sender;
        }

        private string _type;
        public string Type
        {
            get => _type;
        }

        public MessageData(Module Sender, string Type, Object Data)
        {
            _sender = Sender;
            _type = Type.ToLower();
            _data = Data;
            _dataType = Data.GetType();
        }
    }
}
