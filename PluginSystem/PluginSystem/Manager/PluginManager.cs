﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PluginInterface;
using System.IO;
using System.Reflection;
using PluginCommunication;

namespace PluginManager
{
    public class Manager
    {
        private Messenger _messenger;
        public Messenger Messenger => _messenger;

        private List<IPlugin> _loadetPlugins;
        public List<IPlugin> LoadetPlugins
        {
            get
            {
                return _loadetPlugins;
            }
        }
        public int PluginCount 
        { 
            get 
            {
                return _loadetPlugins.Count;
            }
        }
        public event EventHandler<ErrorData> Error;
        private string _pluginFolder;
        private bool _initialized;

        public Manager()
        {
            _loadetPlugins = new List<IPlugin>();
            _pluginFolder = string.Empty;
            _messenger = new Messenger();
            _initialized = false;
        }

        public bool Initialize(string PluginRootFolder)
        {
            _pluginFolder = PluginRootFolder;
            if (!Directory.Exists(_pluginFolder))
            {
                ErrorData errordata = new ErrorData(String.Format("Can't find root folder: {0}", PluginRootFolder), ErrorLevel.Critical);
                TriggerError(errordata);
            }
            else
                _initialized = true;
            return _initialized;
        }

        public bool LoadPlugins()
        {
            if (!_initialized)
            {
                ErrorData errordata = new ErrorData(String.Format("Please call Initialize() first!"), ErrorLevel.Critical);
                TriggerError(errordata);
                return false;
            }
            string[] pluginFiles = getAllDLLs();
            for (int i = 0; i < pluginFiles.Length; i++)
            {
                string currentPath = pluginFiles[i];
                Assembly currentAssembly = null;
                if (!File.Exists(currentPath))
                    continue;

                try
                {
                    currentAssembly = Assembly.LoadFile(currentPath);
                }
                catch (Exception ex)
                {
                    ErrorData errordata = new ErrorData(String.Format("Can't load plugin path: {0}", currentPath), ErrorLevel.Critical, ex);
                    TriggerError(errordata);
                }

                if (currentAssembly == null)
                    continue;

                Type[] allTypes = null;
                try
                {
                    allTypes = currentAssembly.GetTypes();
                }
                catch (Exception ex)
                {
                    ErrorData errordata = new ErrorData(String.Format("Can't load plugins there a one or more broken Assembies: {0}", currentPath), ErrorLevel.Critical, ex);
                    TriggerError(errordata);
                    continue;
                }
                

                foreach (Type currentType in allTypes)
                {
                    if (currentType.IsAbstract || currentType.IsInterface)
                        continue;
                    try
                    {
                        IPlugin currentplugin = (IPlugin)Activator.CreateInstance(currentType);

                        if (AlreadyContains(currentplugin.UniqueName) || !currentplugin.initialize())
                        {
                            ErrorData errordata = new ErrorData(String.Format("plugin {0} already loadet or initialize failed!", currentplugin.DisplayName), ErrorLevel.Critical);
                            TriggerError(errordata);
                        }
                        else
                        {
                            currentplugin.Error += new EventHandler<ErrorData>(currentplugin_Error);
                            _loadetPlugins.Add(currentplugin);
                            _messenger.Register(currentplugin.ComModule);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorData errordata = new ErrorData(String.Format("Error loading plugin: {0}", currentPath), ErrorLevel.Critical, ex);
                        TriggerError(errordata);
                    }
                }
            }

            return true;
        }

        void currentplugin_Error(object sender, ErrorData e)
        {
            TriggerError(e, sender);
        }

        private string[] getAllDLLs()
        {
            string[] AllFiles = Directory.GetFiles(_pluginFolder);
            List<string> DllFiles = new List<string>();

            for (int i = 0; i < AllFiles.Length; i++)
            {
                FileInfo FI = new FileInfo(AllFiles[i]);
                if (FI.Extension.ToLower() == ".dll")
                    DllFiles.Add(FI.FullName);
            }
            return DllFiles.ToArray();
        }

        private bool AlreadyContains(string UniqueName)
        {
            if (_loadetPlugins.Count == 0)
                return false;

            for (int i = 0; i < _loadetPlugins.Count; i++)
            {
                IPlugin currentPlugin = _loadetPlugins[i];
                if (currentPlugin.UniqueName == UniqueName)
                    return true;
            }
            return false;
        }

        private bool TriggerError(ErrorData data, object sender = null)
        {
            EventHandler<ErrorData> handler = Error;
            object esender = sender;
            if (esender == null)
                esender = this;
            if (handler != null)
            {

                handler(esender, data);
                return true;
            }
            return false;
        }

        public IPlugin GetPluginByName(string Name)
        {
            for (int i = 0; i < _loadetPlugins.Count; i++)
            {
                if (_loadetPlugins[i].UniqueName == Name)
                    return _loadetPlugins[i];
            }
            return null;
        }
    }
}
