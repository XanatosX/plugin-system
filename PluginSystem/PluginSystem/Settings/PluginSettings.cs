﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginInterface
{
    public interface IPluginSettings
    {
        HashSet<IPluginSetting> AllSettings { get; }

        bool AddValue(IPluginSetting newSettings);
        void UpdateValue(string key, object Value);
        

        object GetValue(string key);
        bool GetBoolValue(string key);
        int GetIntValue(string key);
        float GetFloatValue(string key);
        string GetStringValue(string key);
        IPluginSetting GetSetting(string key);
    }

    public class PluginSettings : IPluginSettings
    {
        private HashSet<IPluginSetting> _settings;
        public HashSet<IPluginSetting> AllSettings => _settings;

        public PluginSettings()
        {
            _settings = new HashSet<IPluginSetting>();
        }

        public bool AddValue(IPluginSetting newSettings)
        {
            if (GetSetting(newSettings.Key) != null)
                return false;
            _settings.Add(newSettings);
            return true;
        }

        public bool GetBoolValue(string key)
        {
            IPluginSetting curSettings = GetSetting(key);
            if (curSettings.objectType == typeof(bool))
                return (bool)curSettings.Value;

            return false;
        }

        public float GetFloatValue(string key)
        {
            IPluginSetting curSettings = GetSetting(key);
            if (curSettings.objectType == typeof(float))
                return (float)curSettings.Value;

            return 0f;
        }

        public int GetIntValue(string key)
        {
            IPluginSetting curSettings = GetSetting(key);
            if (curSettings.objectType == typeof(int))
                return (int)curSettings.Value;

            return 0;
        }

        public IPluginSetting GetSetting(string key)
        {
            foreach (IPluginSetting settings in _settings)
            {
                if (settings.Key == key)
                    return settings;
            }
            return null;
        }

        public string GetStringValue(string key)
        {
            IPluginSetting curSettings = GetSetting(key);
            if (curSettings.objectType == typeof(string))
                return (string)curSettings.Value;

            return "";
        }

        public object GetValue(string key)
        {
            return GetSetting(key).Value;
        }

        public void UpdateValue(string key, object Value)
        {
            IPluginSetting curSettings = GetSetting(key);
            curSettings.UpdateValue(Value);
        }
    }
}
