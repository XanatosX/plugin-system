﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginInterface
{
    public interface IPluginSetting
    {
        Type objectType { get; }
        string DisplayName { get; }
        string Key { get; }
        object Value { get; }

        bool UpdateValue(object value);
    }

    public class PluginSetting : IPluginSetting
    {
        private Type _type;
        public Type objectType => _type;

        private string _displayName;
        public string DisplayName => _displayName;

        private string _key;
        public string Key => _key;

        private object _value;
        public object Value => _value;

        public PluginSetting(string key, string displayName, Type objectType, object value)
        {
            _key = key;
            _displayName = displayName;
            _value = value;
            _type = objectType;
            init();
        }

        public PluginSetting(string key, string displayName, object value)
        {
            _key = key;
            _displayName = displayName;
            _value = value;
            _type = value.GetType();
            init();
        }

        private void init()
        {
            _key = _key.Replace(" ", "");
        }

        public bool UpdateValue(object value)
        {
            if (value.GetType() != _type)
                return false;
            _value = value;
            return true;
        }
    }
}
