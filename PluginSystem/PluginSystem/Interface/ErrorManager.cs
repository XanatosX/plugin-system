﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginInterface
{
    public enum ErrorLevel
    { 
        Unknown,
        Warning,
        Error,
        Critical
    }

    public class ErrorData : EventArgs
    {
        private Exception _occuredException;
        public Exception ErrorException
        {
            get
            {
                return _occuredException;
            }
        }
        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
        }
        private ErrorLevel _severityCode;
        public ErrorLevel SeverityCode
        {
            get
            {
                return _severityCode;
            }
        }

        public ErrorData(string message, ErrorLevel level, Exception ex = null)
        {
            _occuredException = ex;
            _message = message;
            _severityCode = level;
        }
    }
}
