﻿using PluginCommunication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginInterface
{
    public interface IPlugin
    {
        //MessageSystem
        Module ComModule { get; }

        //PluginData
        string UniqueName { get; }
        string DisplayName { get; }
        string Description { get; }
        string Author { get; }
        
        Version Version { get; }

        //Manage Data
        PluginContext context { get;}
        event EventHandler<ErrorData> Error;
        bool Active { get; set; }
        bool initialized { get; }

        //Actions
        bool initialize();
        bool Load();
        bool PerformeAction(PluginContext context);
        bool Save();
        bool destroy();
    }

    public interface ISettingPlugin : IPlugin
    {
        PluginSettings Settings { get; }
    }

    public interface PluginContext
    {
        string SaveFile { get; set; }
    }

    public static class Interface
    {
        public static bool ContainsInterface(this IPlugin plugin, Type T)
        {
            Type InterfaceType = plugin.GetType().GetInterface(T.Name);
            return (InterfaceType == null) ? false : true;
        }

        public static bool ContainsInterface(this PluginContext context, Type T)
        {
            Type InterfaceType = context.GetType().GetInterface(T.Name);
            return (InterfaceType == null) ? false : true;
        }
    }
}
